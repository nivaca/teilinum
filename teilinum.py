#! /usr/bin/env python3.6

import sys
import re
import os
from os.path import basename

def usage():
    print(f"Usage: {sys.argv[0]} inputfile.xml")
    print(f"\tDefault output file: inputfile_out.xml")

if len(sys.argv) < 2:
    usage()
    sys.exit(-1)
else:
    input_file = sys.argv[1]

if not os.path.isfile(input_file):
    print(f"Input file {input_file} does not exist!")
    sys.exit(-1)


if not os.path.splitext(input_file)[1] == ".xml":
    print(f"Input file must be an XML file!")
    sys.exit(-1)


output_file = os.path.splitext(input_file)[0] + "_out.xml"

with open(input_file, "r") as f:
    read_data = f.readlines()


# Detect if the document is divided by <cb> or <pb>
for line in read_data:
    m = re.match(r"<cb", line)
    if m:
        division = r"<cb"
        break
    else:
        division = r"<pb"


write_data = []
i = 0

with open(output_file, "w") as f:
    # Detect column breaks to establish slices
    for line in read_data:
        outline = line
        # reset i whenever a new <cb> or <pb> happens
        m = re.match(division, line)
        if m:
            i = 1

        m = re.search(r'<lb ed="\#.+?" n=""\s?\/>', line)
        if m:
            outline = re.sub(r'n=""', f'n="{i}"', line)
            print(outline)
            i += 1

        write_data.append(outline)

    f.writelines(write_data)